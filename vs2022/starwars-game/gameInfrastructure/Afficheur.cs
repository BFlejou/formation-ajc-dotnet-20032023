﻿using game.api.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameInfrastructure
{
    /// <summary>
    /// Implémente l'interface IAffichage pour permettre d'afficher un texte
    /// </summary>
    public class Afficheur : IAfficheur
    {
        private readonly Action<string> afficherInfo;

        public Afficheur(Action<string> afficherinfo)
        {
            this.afficherInfo = afficherinfo;
        }

        /// <summary>
        /// Affiche un texte
        /// </summary>
        /// <param name="info">Le texte à afficher</param>
        public void AfficherInfo(string info)
        {
            afficherInfo(info);
        }
    }
}
