﻿using game.api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sauvegarde
{
    public interface ISauvegarde
    {
        void SaveJson(object o, string path);
        void SaveXml(object o, string path);
        void SaveText(object o, string path);

        void SaveOne(CheckPoint c);
    }
}
