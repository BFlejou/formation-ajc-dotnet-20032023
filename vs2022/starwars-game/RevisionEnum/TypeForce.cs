﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevisionEnum
{
    /// <summary>
    /// Les différents types de forces dans le jeu
    /// </summary>
    public enum TypeForce
    {
        None,
        CoteObscur,
        CoteLumineux
    }
}
