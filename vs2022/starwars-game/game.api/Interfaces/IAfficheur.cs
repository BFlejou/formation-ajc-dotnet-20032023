﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.api.Interfaces
{
    /// <summary>
    /// Contrat à implémenter pour afficher une information
    /// </summary>
    public interface IAfficheur
    {
        /// <summary>
        /// Affiche un texte
        /// </summary>
        /// <param name="info">Le texte a afficher</param>
        void AfficherInfo(string info);
    }
}
