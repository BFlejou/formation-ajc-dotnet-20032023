﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starwars_game.starwars.api
{
    /// <summary>
    /// Une partie du jeu Star Wars
    /// </summary>
    public class Partie
    {
        #region Fields
        private DateTime dateDeb;
        private List<Checkpoint> checkpoints;
        private Player player;
        private List<Ennemi> ennemis;
        #endregion

        #region Properties
        public DateTime DateDeb { get => dateDeb; set => dateDeb = value; }
        public List<Checkpoint> Checkpoints { get => checkpoints; set => checkpoints = value; }
        public Player Player { get => player; set => player = value; }
        public List<Ennemi> Ennemis { get => ennemis; set => ennemis = value; }
        #endregion

        #region Constructors
        public Partie(Player player)
        {
            this.dateDeb = DateTime.Now;
            this.player = player;
            this.ennemis = new List<Ennemi>();
            this.checkpoints = new List<Checkpoint>();
        }
        #endregion

        #region Public methods
        public void AjouterCP()
        {
            checkpoints.Add(new Checkpoint(player));
        }

        public void AjouterEnnemi(Ennemi ennemi)
        {
            ennemis.Add(ennemi);
        }

        public void SupprimerEnnemi(Ennemi ennemi)
        {
            ennemis.Remove(ennemi);
        }
        #endregion
    }
}
