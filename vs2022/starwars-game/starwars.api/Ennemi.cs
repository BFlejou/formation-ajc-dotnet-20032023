﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starwars_game.starwars.api
{
    /// <summary>
    /// Un ennemi du jeu Star Wars
    /// </summary>
    public class Ennemi : Personnage
    {
        #region Fields
        //private Partie game;
        #endregion

        #region Properties
        //public Partie Game { get => game; set => game = value; }
        #endregion

        #region Constructors
        public Ennemi(string name) : base(name)
        {
            //this.game = partie;
        }

        public Ennemi(string name, int hp, int puissance, int resistance) : base(name, hp, puissance, resistance)
        {
            //this.game = partie;
        }

        public Ennemi(string name, int hp, int puissance, int resistance, int coX, int coY) : base(name, hp, puissance, resistance, coX, coY)
        {
            //this.game = partie;
        }
        #endregion

        #region Public methods
        public override void Attaquer(Personnage adversaire)
        {
            base.Attaquer(adversaire);
        }

        public override void SeDefendre(Personnage personnage)
        {
            Console.WriteLine($"Je suis {this.Name} et je vais me defendre contre un personnage ! ! !");
            base.SeDefendre(personnage);
        }
        #endregion
    }
}
