﻿using game.api.Interfaces;
using Newtonsoft.Json;
using sauvegarde;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace game.api
{
    /// <summary>
    /// Représente une partie de jeu, avec un perso, des check points, ...
    /// </summary>
    public class Game
    {
        #region Fields
        public event Action<Game, DateTime> Demarrage;

        private readonly ISauvegarde saver;
        private readonly IAfficheur afficher;
        private readonly ILecteurInfos saisir;
        #endregion

        #region Constructors
        public Game(ISauvegarde sauvegarde, IAfficheur affichage, ILecteurInfos saisie)
        {
            this.saver = sauvegarde;
            this.afficher = affichage;
            this.saisir = saisie;
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Demarrer une nouvelle partie
        /// Crée un nouveau perso, ...
        /// </summary>
        public void Demarrer(BasePersonnage persoPrincipal)
        {
            this.PersoPrincipal = persoPrincipal;
            this.DateDebut = DateTime.Now;
            this.Demarrage?.Invoke(this, this.DateDebut);

            this.PersoPrincipal.JeMeurs += PersoPrincipal_JeMeurs;
        }

        private void PersoPrincipal_JeMeurs(BasePersonnage obj)
        {
            // TODO: utiliser un delegué à la place
            afficher.AfficherInfo("Le perso est mort -- GAME OVER");
            afficher.AfficherInfo("Voulez-vous rejouer ? (O/N)");
            string saisie = saisir.RecupererSaisie();
            if (saisie == "O")
            {
                // TODO: bien penser à reset les valeurs par défaut du joueur
                this.Demarrer(this.PersoPrincipal);
            }
        }

        /// <summary>
        /// Créer un point de sauvegarde
        /// </summary>
        /// <exception cref="NotImplementedException">Plante quand ......</exception>
        public void Sauvegarder()
        {
            if (this.PersoPrincipal != null)
            {
                CheckPoint checkPoint = new(this.Id, this.PersoPrincipal.PointsDeVie);

                this.saver.SaveOne(checkPoint);

                this.CheckPointList.Add(checkPoint);
            }
        }
        #endregion

        #region Properties
        public int Id { get; set; } = 0;

        public List<CheckPoint> CheckPointList { get; set; } = new();

        public DateTime DateCreation { get; private set; } = DateTime.Now;

        public DateTime DateDebut { get; private set; } = DateTime.Now;

        public BasePersonnage? PersoPrincipal { get; private set; }
        #endregion

    }
}
