﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starwars_game.starwars.api
{
    /// <summary>
    /// Un joueur du jeu Star Wars
    /// </summary>
    public class Player : Personnage
    {
        #region Fields
        //private List<Partie> parties;
        #endregion

        #region Properties
        //public List<Partie> Parties { get => parties; set => parties = value; }
        #endregion

        #region Constructors
        public Player(string name) : base(name) 
        { 
            //this.parties = new List<Partie>();  
        }

        public Player(string name, int hp, int puissance, int resistance) : base(name, hp, puissance, resistance)
        {
            //this.parties = new List<Partie>();
        }

        public Player(string name, int hp, int puissance, int resistance, int coX, int coY) : base(name, hp, puissance, resistance, coX, coY)
        {
            //this.parties = new List<Partie>();
        }
        #endregion

        #region Public methods
        public override void Attaquer(Personnage adversaire)
        {
            base.Attaquer(adversaire);
        }

        public override void SeDefendre(Personnage personnage)
        {
            Console.WriteLine($"Je suis {this.Name} et je vais me defendre contre un ennemi ! ! !");
            base.SeDefendre(personnage);
        }
        #endregion
    }
}
