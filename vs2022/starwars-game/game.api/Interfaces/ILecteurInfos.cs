﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.api.Interfaces
{
    /// <summary>
    /// Contrat à implémenter pour récupérer une saisie utilisateur
    /// </summary>
    public interface ILecteurInfos
    {
        /// <summary>
        /// Récupère la saisie de l'utilisateur
        /// </summary>
        /// <returns>La saisie de l'utilisateur</returns>
        string RecupererSaisie();
    }
}
