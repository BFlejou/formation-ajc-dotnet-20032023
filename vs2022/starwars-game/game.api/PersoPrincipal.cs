﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.api
{
    /// <summary>
    /// Personnage principal, avec des spécificités sur l'attaque
    /// </summary>
    public class PersoPrincipal : BasePersonnage
    {
        private readonly Action<string> afficherInformation;
        private readonly Func<string> recupererSaisie;
        private static readonly Dictionary<string, Position2D> deplacements = new()
        {
            { "Z", new (-1, 0) },
            { "S", new (1, 0) },
            { "Q", new (0, -1) },
            { "D", new (0, 1) }
        };

        public PersoPrincipal(Action<string> afficherInformation, Func<string> recupererSaisie)
        {
            this.afficherInformation = afficherInformation;
            this.recupererSaisie = recupererSaisie;
            this.Position = new Position2D(0, 0);
        }

        /// <summary>
        /// Je joueur se déplace avec ZQSD
        /// </summary>
        public override void SeDeplacer()
        {
            this.afficherInformation("Direction ? (ZQSD)");
            string saisie = this.recupererSaisie();
            if (saisie != null)
            {
                var vecteur = this.ConvertirPosition(saisie.ToUpper());
                this.Position = new(this.Position.X + vecteur.X, this.Position.Y + vecteur.Y);
            }
        }

        private Position2D ConvertirPosition(string direction)
        {
            return PersoPrincipal.deplacements[direction];
        }

        protected override int DefinirPointsAttaque()
        {
            var pointsAttaque = base.DefinirPointsAttaque();

            pointsAttaque += this.Bouclier;

            return pointsAttaque;
        }
    }
}
