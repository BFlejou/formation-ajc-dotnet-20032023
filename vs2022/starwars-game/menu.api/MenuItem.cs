﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu.api
{
    /// <summary>
    /// Un item du menu
    /// </summary>
    public class MenuItem
    {
        private readonly Action methode;
        public int Id { get; set; }

        public string Libelle { get; set; } = "";

        public int OrdreAffichage { get; set; }

        public MenuItem(Action action)
        {
            this.methode = action;
        }

        /// <summary>
        /// Exécute la methode assignée à ce MenuItem
        /// </summary>
        public void ExecuterAction()
        {
            this.methode();
        }

        public override string ToString()
        {
            return $"{this.Id} : {this.Libelle}";
        }
    }
}
