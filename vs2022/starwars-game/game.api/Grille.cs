﻿using game.api.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace game.api
{
    /// <summary>
    /// Représente la grille du jeu
    /// </summary>
    public class Grille
    {
        private readonly IAfficheur afficher;
        private int longueur;
        private int largeur;

        public Grille(IAfficheur afficher, int longueur = 10, int largeur = 10)
        {
            this.afficher = afficher;
            this.Longueur = longueur;
            this.Largeur = largeur;
        }

        public int Longueur { get => longueur; set => longueur = value; }
        public int Largeur { get => largeur; set => largeur = value; }

        /// <summary>
        /// Affiche la grille avec le perso principal et les ennemis dedans
        /// </summary>
        /// <param name="player">Le joueur</param>
        /// <param name="ennemiList">La liste des ennemis</param>
        public void AfficherGrille(BasePersonnage player, List<Ennemi> ennemiList)
        {
            for (int i = 0; i < Longueur; i++) 
            {
                afficher.AfficherInfo("\n");
                for (int j = 0; j < Largeur; j++)
                {
                    if (i == player.Position.X && j == player.Position.Y)
                    {
                        afficher.AfficherInfo("[O] ");
                    }
                    else
                    {
                        bool ennemiSurCetteCase = false;
                        foreach (Ennemi ennemi in ennemiList)
                        {
                            if (i == ennemi.Position.X && j == ennemi.Position.Y && !ennemiSurCetteCase)
                            {
                                afficher.AfficherInfo("[X] ");
                                ennemiSurCetteCase = true;
                            }
                        }

                        if (!ennemiSurCetteCase)
                        {
                            afficher.AfficherInfo("[ ] ");
                        }
                    }
                }
            }
            afficher.AfficherInfo("\n");
        }
    }
}
