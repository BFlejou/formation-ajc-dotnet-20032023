﻿using delegates.api;
using game.api;
using gameInfrastructure;
using menu.api;

Random rnd = new Random();
Afficher afficherLigne = new Afficher(Console.WriteLine);
LectureSaisie lectureSaisie = new LectureSaisie(Console.ReadLine);

string cheminSauvegarde = Path.Combine(Environment.CurrentDirectory, "checkpoint.json");

var game = new Game(new JsonGameSaver(cheminSauvegarde), new Afficheur(Console.WriteLine), new LecteurInfos(Console.ReadLine));
BasePersonnage persoPrincipal = new PersoPrincipal(Console.WriteLine, Console.ReadLine);

Menu menu = new(mess =>
{
    Console.ForegroundColor = ConsoleColor.DarkGreen;
    afficherLigne(mess);
    Console.ForegroundColor = ConsoleColor.White;
},
Console.ReadLine);

menu.Ajouter(new MenuItem(CommencerPartie) { Id = 1, Libelle = "Nouvelle partie", OrdreAffichage = 1 });
menu.Ajouter(new MenuItem(ChargerPartie) { Id = 2, Libelle = "Charger partie", OrdreAffichage = 2 });
menu.Ajouter(new MenuItem(Quitter) { Id = 0, Libelle = "Quitter le jeu", OrdreAffichage = 3 });

game.Demarrage += (Game arg1, DateTime arg2) =>
{
    afficherLigne("Démarrage de la partie !");
};

Console.ForegroundColor = ConsoleColor.DarkGreen;

var monTitre = "a starwars game".ToUpper();

afficherLigne($"{monTitre.ToUpper()} ({DateTime.Now.ToString("yyyy.MM.dd dddd")}) !");
Console.ForegroundColor = ConsoleColor.DarkCyan;
var sousTitre = "A zelda copy cat";
afficherLigne(sousTitre);
Console.ForegroundColor = ConsoleColor.White;


ChoisirMenu();

void ChoisirMenu()
{
    int choix = 10;

    while (choix != 0)
    {
        menu.Afficher();
        afficherLigne("Ton choix ?");
        if (int.TryParse(lectureSaisie(), out choix))
        {
            menu.Items.Find(item => item.Id == choix)?.ExecuterAction();
        }
        else
        {
            afficherLigne("Choix non valide");
            choix = 10;
        }
    }
}


void CommencerPartie()
{
    game.Demarrer(persoPrincipal);
    Grille grille = new Grille(new Afficheur(Console.Write));

    List<Ennemi> ennemis = new()
{
    new Ennemi(),
    new Ennemi(),
    new Ennemi()
};

    grille.AfficherGrille(persoPrincipal, ennemis);

    // Je vais attaquer

    while (persoPrincipal.EstVivant)
    {
        persoPrincipal.SeDeplacer();
        foreach (var e in ennemis)
        {
            e.SeDeplacer();
        }
        grille.AfficherGrille(persoPrincipal, ennemis);

        persoPrincipal.PerdreVie(5);
    }
}

void ChargerPartie()
{
    throw new NotImplementedException();
}

void Quitter()
{
    try
    {
        game.Sauvegarder();
        // Je ferme
    }
    catch (NotImplementedException ex)
    {
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine("Oops erreur !", ex.Message);
        Console.ForegroundColor = ConsoleColor.White;
    }
    catch (Exception ex)
    {

    }
    finally
    {
        afficherLigne("Execute obligatoire");
    }
}

//Archives

//void DemarrerPartie()
//{
//    afficherLigne("Ton prénom stp ?");
//    var prenom = Console.ReadLine();

//    // DateTime maVraiDate;
//    bool dateValide = false;
//    do
//    {
//        afficherLigne("Ta date de naissance stp ?");
//        var dateDeNaissance = lectureSaisie();

//        if (DateTime.TryParse(dateDeNaissance, out var maVraiDate))
//        {
//            var comparaisonDates = DateTime.Now - maVraiDate;
//            int nbAnnees = (int)(comparaisonDates.TotalDays / 365);
//            afficherLigne($"Ton prénom est bien ? {prenom}, et tu es âgé de {nbAnnees} années ?");

//            dateValide = true;
//            int ageSaisi = DemanderAge();
//        }
//        else
//        {
//            Console.ForegroundColor = ConsoleColor.DarkRed;
//            afficherLigne("Erreur de saisie de date, ré-essaie stp !");
//            Console.ForegroundColor = ConsoleColor.White;
//        }
//    }
//    while (!dateValide);
//}

//int DemanderAge()
//{
//    bool ageValide = false;
//    int vraiAge = 0;

//    while (!ageValide)
//    {
//        afficherLigne("Ton age stp ?");
//        var age = lectureSaisie();

//        ageValide = int.TryParse(age, out vraiAge);

//        if (ageValide)
//        {
//            ageValide = vraiAge > 13;
//        }
//    }

//    return vraiAge;
//}

////PreparerEnnemis();

//void PreparerEnnemis()
//{
//    string[,] multiDimensionnel = new string[2, 3];
//    var item = multiDimensionnel[0, 0];

//    List<string> noms = new List<string>()
//    {
//        "Dark Vador",
//        "Boba fet",
//        "Empereur Palpatine",
//    };
//    DemandeAjoutEnnemisParUtilisateur(noms);

//    var query = from enem in noms
//                let eneMaj = enem[0].ToString().ToUpper() + enem.Substring(1).ToLower()
//                let premierCar = enem[0]
//                let monObjet = new { Maj = eneMaj, Initial = enem }
//                where enem.StartsWith("D") || enem.EndsWith("D")
//                orderby enem descending
//                select monObjet;

//    foreach (var enemy in query)
//    {
//        Console.WriteLine(enemy.Maj);
//        Console.WriteLine(enemy.Initial);
//    }
//}

//void DemandeAjoutEnnemisParUtilisateur(List<string> lesEnnemis)
//{
//    const string ARRET_SAISIE = "STOP";
//    string valeurSaisie = "";
//    bool demandeArret = false;

//    do
//    {
//        afficherLigne("Nouvel ennemi ? (STOP pour arrêter)");
//        valeurSaisie = lectureSaisie();
//        demandeArret = valeurSaisie == ARRET_SAISIE;

//        if (!demandeArret && !lesEnnemis.Contains(valeurSaisie))
//        {
//            lesEnnemis.Add(valeurSaisie);
//        }

//    } while (!demandeArret);

//}
