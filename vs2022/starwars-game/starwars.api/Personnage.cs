﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starwars_game.starwars.api
{
    /// <summary>
    /// Un personnage du jeu Star Wars
    /// </summary>
    public abstract class Personnage
    {
        #region Fields
        private string name = "";
        private int pv;
        private int puissance;
        private int resistance;
        private int coorX;
        private int coorY;
        #endregion

        #region Properties
        public string Name { get => name; set => name = value; }
        public int PV { get => pv; set => pv = value; }
        public int CoorX { get => coorX; set => coorX = value; }
        public int CoorY { get => coorY; set => coorY = value; }
        public int Puissance { get => puissance; set => puissance = value; }
        public int Resistance { get => resistance; set => resistance = value; }
        #endregion

        #region Constructors
        public Personnage(string name) : this(name, 100, 10, 5) 
        { 
            this.name = name; 
        }

        public Personnage(string name, int hp, int puissance, int resistance) : this(name, hp, puissance, resistance, 0, 0)
        {
            this.name = name;
            this.pv = hp;
            this.puissance = puissance;
        }

        public Personnage(string name, int hp, int puissance, int resistance, int coX, int coY)
        {
            this.name = name;
            this.pv = hp;
            this.puissance = puissance;
            this.resistance = resistance;
            this.coorX = coX;
            this.coorY = coY;
        }
        #endregion

        #region Public methods
        public virtual void Attaquer(Personnage adversaire)
        {
            Console.WriteLine($"{this.name} attaque {adversaire.Name} ! ! !");
            adversaire.PV -= this.puissance - adversaire.Resistance;
        }

        public virtual void SeDefendre(Personnage personnage) 
        {
            Console.WriteLine("Je me defends ! ! !");
        }
        #endregion
    }
}
