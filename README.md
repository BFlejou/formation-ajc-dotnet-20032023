Le projet à éxécuter est starwars-game.

Les classes métier utilisées sont dans la bibliothèque de classes game.api, avec les classes 
 - Game
 - BasePersonnage
 - PersoPrincipal
 - Ennemi
 - CheckPoint
 - Grille
ainsi que les interfaces ISauvegarde, IAfficheur et ILecteurInfos.

Ces deux dernières sont implémentées respectivement par les classes Afficheur et LecteurInfos situées 
dans la bibliothèque de classes gameInfrastructure qui contient également les classes de sauvegarde implémentant ISauvegarde.

Enfin, les classes Menu et MenuItem correspondant au menu du jeu sont situées dans la bibliothèque de classes menu.api.

Le diagramme de classes est le fichier ClassDiagramStarwarsGame.svg.

Je n'ai pas beaucoup eu le temps de travailler ce week-end donc j'ai seulement fait les deux premières parties de l'énoncé.
