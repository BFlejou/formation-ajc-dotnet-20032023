﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starwars_game.starwars.api
{
    /// <summary>
    /// Un point de sauvegarde d'une partie de Star Wars
    /// </summary>
    public class Checkpoint
    {
        private DateTime date;
        private int playerPv;
        private int playerCoX;
        private int playerCoY;


        #region Properties
        public DateTime Date { get => date; set => date = value; }
        public int PlayerPv { get => playerPv; set => playerPv = value; }
        public int PlayerCoX { get => playerCoX; set => playerCoX = value; }
        public int PlayerCoY { get => playerCoY; set => playerCoY = value; }

        #endregion

        #region Constructors
        public Checkpoint(Player player)
        {
            this.date = DateTime.Now;
            this.playerPv = player.PV;
            this.playerCoX = player.CoorX;
            this.playerCoY = player.CoorY;
        }
        #endregion

        #region Public methods

        #endregion
    }
}
