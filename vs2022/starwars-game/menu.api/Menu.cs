﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu.api
{
    /// <summary>
    /// Menu avec ses item
    /// </summary>
    public class Menu
    {
        #region Fields
        private readonly Action<string> afficherInfo;
        private readonly Func<string> recupererInfo;
        #endregion

        #region Constructors
        public Menu(Action<string> afficher, Func<string> recupererInfo)
        {
            this.afficherInfo = afficher;
            this.recupererInfo = recupererInfo;
        }
        #endregion

        #region Public methods

        /// <summary>
        /// Affichage de tous les items du menu
        /// </summary>
        public void Afficher()
        {
            foreach (var item in this.Items.OrderBy(item => item.OrdreAffichage))
            {
                this.afficherInfo(item.ToString());
            }
        }

        /// <summary>
        /// Ajout d'un item au menu
        /// </summary>
        /// <param name="item">Le MenuItem à ajouter</param>
        public void Ajouter(MenuItem item)
        {
            this.Items.Add(item);
        }
        #endregion

        #region Properties
        public List<MenuItem> Items { get; private set; } = new();
        #endregion
    }
}
