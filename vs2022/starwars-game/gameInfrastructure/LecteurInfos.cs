﻿using game.api.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gameInfrastructure
{
    /// <summary>
    /// Implémente l'interface Isaisie pour permettre de récupérer une saisie de l'utilisateur
    /// </summary>
    public class LecteurInfos : ILecteurInfos
    {
        private readonly Func<string> recupererInfo;

        public LecteurInfos(Func<string> recupInfo)
        {
            this.recupererInfo = recupInfo;
        }

        /// <summary>
        /// Récupère la saisie de l'utilisateur
        /// </summary>
        /// <returns>La saisie de l'utilisateur</returns>
        public string RecupererSaisie()
        {
            return recupererInfo();
        }
    }
}
