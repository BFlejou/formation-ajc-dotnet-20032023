﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delegates.api
{
    /// <summary>
    /// Contrat de méthode pour afficher un item
    /// </summary>
    /// <param name="item"></param>
    public delegate void Afficher(object item); // le quoi: la signature de votre méthode, c'est un nouveau type

    /// <summary>
    /// Contrat de méthode pour lire une données Utilisateur
    /// </summary>
    /// <returns></returns>
    public delegate string? LectureSaisie();

}
